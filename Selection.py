from random import randint

lst = [randint(-35, 54) for e in range(100)]


def selection_sort(arr, reverse=None):

    for i in range(len(arr)-1):
        for k in range(i+1, len(arr)):
            if arr[k] < arr[i]:
                arr[k], arr[i] = arr[i], arr[k]

    if reverse:
        return arr[::-1]
    else:
        return arr

