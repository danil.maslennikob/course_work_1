from random import randint

lst = [randint(-35, 54) for e in range(100_000)]


def shell_sort(data, reverse=None):
    last_index = len(data) - 1
    step = len(data)//2
    while step > 0:
        for i in range(step, last_index + 1, 1):
            j = i
            delta = j - step
            while delta >= 0 and data[delta] > data[j]:
                data[delta], data[j] = data[j], data[delta]
                j = delta
                delta = j - step
        step //= 2
    if reverse:
        return data[::-1]
    else:
        return data


print(lst)
print(shell_sort(lst))