import numpy as np
import matplotlib.pyplot as plt
from quick import quicksort
from scipy.optimize import curve_fit
import sys
from fun import elapsed_time


sys.setrecursionlimit(5000)


xdata = []
ydata = []

step = 100
min_size = 100
max_size = 1000


for n in range(min_size, max_size + step, step):
    xdata.append(n)
    x = sorted([e for e in range(n)], reverse=True)
    ydata.append(elapsed_time(quicksort, x))


print(xdata)
print(ydata)

xdata = np.array(xdata)
ydata = np.array(ydata)


def func(n, a, b, c):
    return a * n * (np.log(b * n)) + c


popt, pcov = curve_fit(func, xdata, ydata)
print(popt)

plt.style.use('bmh')
fig, ax = plt.subplots()


ax.set_title('Быстрая сортировка', fontsize=22, c='black')
ax.set_xlabel('Размер массива (элементы)', fontsize=14, c='black')
ax.set_ylabel('Время выполнения (сек)', fontsize=14, c='black')


ax.plot(xdata, ydata, 'b-', label='data')
ax.plot(xdata, func(xdata, *popt), 'r-', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
plt.plot(np.linspace(1_000, 5_000, 1_000), func(np.linspace(1_000, 5_000, 1_000), *popt), 'g-',
         label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))

plt.legend()
plt.show()
