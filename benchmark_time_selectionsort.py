import numpy as np
import matplotlib.pyplot as plt
from Selection import selection_sort
from fun import elapsed_time


xdata = []
ydata = []

step = 1000
min_size = 1000
max_size = 10000

for n in range(min_size, max_size + step, step):
    xdata.append(n)
    x = sorted([e for e in range(n)], reverse=True)
    ydata.append(elapsed_time(selection_sort, x))




xdata = np.array(xdata)
ydata = np.array(ydata)


x = np.array(xdata)
y = np.array(ydata)
z = np.polyfit(x, y, 2)
f = np.poly1d(z)

plt.style.use('bmh')
fig, ax = plt.subplots()

ax.set_title('Сортировка выбором', fontsize=22, c='black')
ax.set_xlabel('Размер массива (элементы)', fontsize=14, c='black')
ax.set_ylabel('Время выполнения (сек)', fontsize=14, c='black')

ax.plot(xdata, ydata, 'r-', label='data')
ax.plot(xdata, ydata, 'b-', label='prognosis')

x1 = np.linspace(10_000, 50_000, 10_000)
plt.plot(x1, f(x1))
plt.plot(x, y)
plt.legend()
plt.show()
