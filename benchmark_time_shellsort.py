import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from Shell import shell_sort
from fun import elapsed_time


xdata = []
ydata = []

step = 1000
min_size = 1000
max_size = 10000

for n in range(min_size, max_size + step, step):
    xdata.append(n)
    x = sorted([e for e in range(n)], reverse=True)
    ydata.append(elapsed_time(shell_sort, x))




xdata = np.array(xdata)
ydata = np.array(ydata)


def func(n, a, b, c):
    return a * n * (np.log(b * n) ** 2) + c


popt, pcov = curve_fit(func, xdata, ydata)



plt.style.use('bmh')
fig, ax = plt.subplots()

ax.set_title('Сортирова Шелла', fontsize=22, c='black')
ax.set_xlabel('Размер массива (элементы)', fontsize=14, c='black')
ax.set_ylabel('Время выполнения (сек)', fontsize=14, c='black')

ax.plot(xdata, ydata, 'b-', label='Входные данные')
ax.plot(xdata, func(xdata, *popt), 'r-', label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))
plt.plot(np.linspace(10_000, 50_000, 10_000), func(np.linspace(10_000, 50_000, 10_000), *popt), 'g-',
         label='fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt))

plt.legend()
plt.show()
