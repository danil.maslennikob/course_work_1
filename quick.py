from random import randint

lst = [randint(-35, 54) for e in range(100_000)]


def quicksort(array, reverse=None):
    # Если входной массив содержит менее двух элементов,
    # тогда вернуть его как результат функции
    if len(array) < 2:
        return array

    low, same, high = [], [], []

    # Случайным образом выберите свой сводный
    pivot = array[randint(0, len(array) - 1)]

    for item in array:
        # Элементы, которые меньше "опорного элемента",
        # переходят в нижний список. Элементы, которые
        # больше "опорного элемента", переходят в верхний
        # список. Элементы, которые равны "опорному элементу"
        # опадают в тот же список.
        if item < pivot:
            low.append(item)
        elif item == pivot:
            same.append(item)
        elif item > pivot:
            high.append(item)

    # Финальный результат объединяет нижний список с
    # "опорным элементом" и верхним списком
    if not reverse:
        return quicksort(low) + same + quicksort(high)
    elif reverse:
        return list(reversed(quicksort(low) + same + quicksort(high)))

# Выводит сначала неосортированный список и отсартированный список
# print(lst)
# print(quicksort(lst))
